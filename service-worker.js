self.addEventListener('fetch', function(event) {
  var requestUrl = new URL(event.request.url);
  console.log('Handling fetch event for', event.request.url, 'with pathname', requestUrl.pathname);

  if (requestUrl.pathname === '/link2.js') {

    var responseBody = {
      "hello": "world"
    };

    var responseInit = {
      status: 200,
      statusText: 'OK',
      headers: {'Content-Type': 'application/json'}
    };

    var mockResponse = new Response(JSON.stringify(responseBody), responseInit);

    console.log(' Responding with a mock response body:', responseBody);
    event.respondWith(mockResponse);
  }
});